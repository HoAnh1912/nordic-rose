import asset from "plugins/assets/asset";

export default function Articles() {
  return (
    <div className="articles">
      <div className="container">
        <div className="articles__title">
          <h3>All articles</h3>
        </div>
        <div className="articles__wrap">
          <a className="item" href="/articles-detail">
            <img src={asset("/images/home/Rectangle 12-1.png")} />
            <p>Here are some things you should know regarding how we work </p>
          </a>
          <a className="item" href="/articles-detail">
            <img src={asset("/images/home/Rectangle 13-1.png")} />
            <p>
              Granny gives everyone the finger, and other tips from OFFF
              Barcelona
            </p>
          </a>
          <a className="item" href="/articles-detail.js">
            <img src={asset("/images/home/Rectangle 13.png")} />
            <p>Hello world, or, in other words, why this blog exists </p>
          </a>
          <a className="item" href="/articles-detail">
            <img src={asset("/images/home/Rectangle 12.png")} />
            <p>Here are some things you should know regarding how we work </p>
          </a>
          <a className="item" href="/articles-detail">
            <img src={asset("/images/home/Rectangle 14.png")} />
            <p>
              Connecting artificial intelligence with digital product design
            </p>
          </a>
          <a className="item" href="/articles-detail">
            <img src={asset("/images/home/Rectangle 15.png")} />
            <p>It’s all about finding the perfect balance</p>
          </a>
          <a className="item" href="/articles-detail">
            <img src={asset("/images/home/Rectangle 12-2.png")} />
            <p>I believe learning is the most important skill </p>
          </a>
          <a className="item" href="/articles-detail">
            <img src={asset("/images/home/Rectangle 13-2.png")} />
            <p>Clients are part of the team</p>
          </a>
          <a className="item" href="/articles-detail">
            <img src={asset("/images/home/Rectangle 19.png")} />
            <p>Clients are part of the team</p>
          </a>
          <a className="item" href="/articles-detail">
            <img src={asset("/images/home/Rectangle 17.png")} />
            <p>Here are some things you should know regarding how we work </p>
          </a>
          <a className="item" href="/articles-detail">
            <img src={asset("/images/home/Rectangle 16.png")} />
            <p>
              Connecting artificial intelligence with digital product design{" "}
            </p>
          </a>
          <a className="item" href="/articles-detail">
            <img src={asset("/images/home/Rectangle 18.png")} />
            <p>
              How modern remote working tools get along with Old School Cowboy's
              methods{" "}
            </p>
          </a>
        </div>
      </div>
    </div>
  );
}
