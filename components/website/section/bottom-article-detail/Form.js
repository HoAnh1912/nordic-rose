import React from "react";

export default function Form() {
  return (
    <section class="form">
      <div class="container">
        <div class="form__wrap">
          <div class="form-text">
            <h2>Sign up for the newsletter</h2>
            <p>
              If you want relevant updates occasionally, sign up for the private
              newsletter. Your email is never shared.
            </p>
          </div>
          <form data-members-form="subscribe">
            <div className="form-input">
              <input
                data-members-email=""
                type="email"
                placeholder="Enter your email..."
                autocomplete="false"
              />
              <button type="submit">
                <span>Sign up</span>
              </button>
            </div>
          </form>
        </div>
      </div>
    </section>
  );
}
