import asset from "plugins/assets/asset";
import useToggle from "ahooks/lib/useToggle";
import { useState } from "react";
// // const [showMe] = false;
// const toggle = () => {
//   // return true;
// };
// const [personState, setPersonState] = React.useState(false);
// // function toggle() {
// //   setShowMe(!showMe);
// // }
const mobile = {
  Name: false,
};
export default function Header() {
  const [showMenuList, showMenuListAction] = useToggle(false);
  const handleMenuTogglerClick = (event) => {
    if (event) event.preventDefault();

    showMenuListAction.toggle();
  };
  return (
    <div className="header">
      <div className="container-fluid">
        <div className="logo">
          <a href="#">
            <img src={asset("/images/home/logo_dark.svg")} />
          </a>
        </div>
        <ul className="nav">
          <li>
            <a href="/">BLOG</a>
          </li>
          <li>
            <a href="/about">ABOUT</a>
          </li>
          <li>
            <a href="/links">LINKS</a>
          </li>
          <li>
            <a href="/projects">PROJECTS</a>
          </li>
        </ul>
        <div className={`btnmenu`} onClick={handleMenuTogglerClick}>
          <span></span>
        </div>

        <nav className={`menu-mobile ${showMenuList ? "active" : ""}`}>
          <ul>
            <li>
              <a href="/">BLOG</a>
            </li>
            <li>
              <a href="/about">ABOUT</a>
            </li>
            <li>
              <a href="/links">LINKS</a>
            </li>
            <li>
              <a href="/projects">PROJECTS</a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
}
