import ButtonNoneBorder from "../button/ButtonNoneBorder";
import ButtonRound from "../button/ButtonRound";
import COLOR from "../constants/Color";
import FONTS from "../constants/Font";
import asset from "@/plugins/assets/asset";
import { TweenMax, TimelineLite, gsap } from "gsap";
import { useInView } from "react-intersection-observer";
import { useState, useEffect, useRef, useContext } from "react";

export default function BannerItem({
  data,
  textColorButton,
  iconButton,
  onClickVideo,
  classNames,
}) {
  const imgRef = useRef();
  const textRef = useRef();

  const { ref, inView, entry } = useInView({
    /* Optional options */
    threshold: 0,
  });

  const tl1 = new TimelineLite();
  let timeout;

  const animationOn = function () {
    timeout = setTimeout(() => {
      // TweenMax.set(textRef.current, { x: 0, autoAlpha: 1 }, 1);
      gsap.to(imgRef.current, {
        duration: 0.6,
        x: 0,
        autoAlpha: 1,
      });
      gsap.to(textRef.current, {
        duration: 0.6,
        x: 0,
        autoAlpha: 1,
      });
    }, 200);
  };

  const animationOff = function () {
    gsap.to(imgRef.current, 0, { x: 50, autoAlpha: 0 }, 0.1);
    gsap.to(textRef.current, 0, { x: 0, autoAlpha: 0 }, 0.1);
    clearTimeout(timeout);
  };

  useEffect(() => {
    if (inView === true && textRef.current) {
      animationOn();
    } else {
      animationOff();
    }
  }, [inView]);

  return (
    <div className={"itemImageBanner " + classNames}>
      <div className="textItem" ref={textRef}>
        <span ref={ref} className="viewPoint"></span>
        <p className="subTop aniText">{data?.subTop}</p>
        {data?.hasTag && (
          <p className="hasTag aniText">
            <span>{data.hasTag}</span>
          </p>
        )}
        <h2 className="aniText">
          {data?.titleLeft ? (
            <span className="titleLeft">{data.titleLeft + " "}</span>
          ) : (
            ""
          )}
          <span>{data.titleRight}</span>
        </h2>
        <p className="description aniText">{data?.description}</p>
        {data?.video ? (
          <ButtonRound
            onClick={() => onClickVideo(data.video)}
            widthIcon="35px"
            icon={asset("/images/icon-play.png")}
          />
        ) : (
          ""
        )}
        {data?.href ? (
          <ButtonNoneBorder icon={iconButton} textColor={textColorButton} />
        ) : (
          ""
        )}
      </div>
      <div
        ref={imgRef}
        className="imageItem"
        style={{ backgroundImage: `url(${data?.img})`, height: "500px" }}
      >
        <img src={asset("/images/demo/banner-thumb-transfer.png")} />
        <p className="buttonPlay">
          {data?.video ? (
            <ButtonRound
              onClick={() => onClickVideo(data.video)}
              widthIcon="35px"
              icon={asset("/images/icon-play.png")}
            />
          ) : (
            ""
          )}
        </p>
      </div>
      <style jsx>{`
        .buttonPlay {
          display: none;
        }
        .itemImageBanner.dark {
          .textItem {
            color: ${COLOR.WHITE};
            h2 {
              color: ${COLOR.WHITE};
            }
          }
        }
        .itemImageBanner {
          display: flex;
          position: relative;
          padding-left: 42%;
          padding-bottom: 25px;
          .viewPoint {
            position: absolute;
            left: 50%;
            top: 50%;
            width: 0;
            height: 0;
          }
          .textItem {
            opacity: 0;
            position: absolute;
            top: 50%;
            left: 0;
            transform: translate(0, -50%);
            padding-left: 1px;
            width: 49%;
            min-width: 300px;
            transition: 0.3s;
            z-index: 2;
            h2 {
              font-size: 35px;
              font-family: ${FONTS.AvenirNextLTProBold};
              /* max-width: 50%; */
              padding: 18px 0;
              transition: 0.3s;
            }
          }
          .hasTag {
            padding-top: 10px;
            display: flex;
            justify-content: flex-start;
            align-items: center;
            span {
              background-color: ${COLOR.TEXT_COLOR_SPECIAL};
              color: ${COLOR.WHITE};
              padding: 8px 18px;
              border-radius: 25px;
              font-size: 12px;
              padding-top: 10px;
              font-family: ${FONTS.AvenirNextLTProBold};
              text-transform: uppercase;
              display: inline-block;
            }
          }
          .subTop {
            text-transform: uppercase;
            font-size: 14px;
          }
          .description {
            font-size: 18px;
            max-width: 525px;
            padding-bottom: 10px;
          }
          .titleLeft {
            color: ${COLOR.TEXT_COLOR_SPECIAL};
          }
          .imageItem {
            flex: 1;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            border-radius: 20px;
            img {
              width: 100%;
            }
          }
          img {
            width: 100%;
            opacity: 0;
          }
        }
        @media only screen and (max-width: 768px) {
          .buttonPlay {
            position: absolute;
            margin-left: auto;
            display: flex;
            justify-content: flex-end;
            right: 3px;
            top: 92%;
          }
          .itemImageBanner {
            flex-direction: column;
            padding-left: 0;
            padding-top: 10px;
            padding-bottom: 0;
            .textItem {
              position: relative;
              width: 100%;
              top: unset;
              left: 0;
              transform: unset;
              order: 2;
              padding-top: 20px;
            }
            .imageItem {
              width: 100%;
              order: 1;
              position: relative;
            }
          }
        }
      `}</style>
    </div>
  );
}
