import asset from "plugins/assets/asset";

export default function Footer() {
  return (
    <footer className="site-footer">
      <div className="slide">
        <span>Digital product design</span>
        <span>Remote work</span>
        <span>UX design</span>
        <span>Distributed teams</span>
        <span>Creativity</span>
        <span>Strategy</span>
        <span>Suspense</span>
        <span>Growth</span>
      </div>
      <div className="container">
        <div className="content">
          <a href="#">
            <img src={asset("/images/home/logo_light.svg")} />
          </a>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu
            velit tempus erat egestas efficitur. In hac habitasse platea
            dictumst. Fusce a nunc eget ligula suscipit finibus.
          </p>
          <ul class="address">
            <li>
              <a
                href="https://twitter.com/mikamatikainen"
                target="_blank"
                rel="noopener"
              >
                Twitter
              </a>
            </li>
            <li>
              <a
                href="https://www.linkedin.com/in/mikamatikainen"
                target="_blank"
                rel="noopener"
              >
                LinkedIn
              </a>
            </li>
            <li>
              <a
                href="https://www.nordicrose.net/blog/rss/"
                target="_blank"
                rel="noopener"
              >
                RSS
              </a>
            </li>
          </ul>
          <p class="site-footer__bottom-text">
            © 2012-2021 Nordic Rose Co.
            <br />
            All rights reserved.
          </p>
        </div>
      </div>
    </footer>
  );
}
