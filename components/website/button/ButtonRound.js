import asset from "plugins/assets/asset";
import { COLOR, LOGO } from "components/website/constants/Constants";

const color = COLOR;

export default function ButtonRound({
    icon,
    children,
    background = color.BLACK,
    backgroundHover = color.BLACK,
    backgroundRound = color.WHITE, 
    borderColorRound = color.BLACK,
    textColor = color.BLACK,
    onClick,
    textTransform,
    classNames,
    type,
    widthIcon="25px"
}) {
    switch (type) {
        case 2:
            return <span className={"btnRound " + classNames} onClick={onClick}>
                <p>{children}</p>
                <i className="btnEffect"></i>
                <style jsx>{`
                    .btnRound{
                        display: block;
                        position: relative;
                        background-color: ${background};
                        border: solid 1px ${color.BLACK};
                        border-radius:100%;
                        color: ${color.WHITE};
                        height:45px;
                        width: 45px;
                        cursor: pointer;
                        overflow: hidden;
                        transition: 0.1s;
                        text-align:center;
                        display:flex;
                        align-items: center;
                        justify-content: center;

                        p {
                            color: ${textColor};
                            text-transform: ${textTransform ? textTransform : "unset"};
                        }

                        &::after{
                            content: "";
                            position: absolute;
                            background-image: url(${icon});
                            background-position: center;
                            background-size: 100%;
                            background-repeat: no-repeat;
                            transition: 0.5s;
                            width: 25px;
                            height: 25px;
                            border-radius:100%;
                            top: 50%;
                            left: 50%;
                            transform: translate(-50%,-50%);
                        }
                        .btnEffect{
                            display: none;
                        }
                    }
                    .btnRound:hover{
                        background-color:${backgroundHover};
                    }
                `}</style>
        </span>

        default:
            return <span className={"btnRound " + classNames} onClick={onClick}>
                <p>{children}</p>
                <i className="btnEffect"></i>
                <style jsx>{`
                .btnRound{
                    display: block;
                    position: relative;
                    background-color: ${background};
                    border: solid 1px ${color.BG_GRAY_LIGHT};
                    border-radius:100%;
                    color: ${color.WHITE};
                    height:45px;
                    width: 45px;
                    cursor: pointer;
                    transition: 0.3s;
                    text-align:center;
                    display:flex;
                    align-items: center;
                    justify-content: center;
                    z-index: 2;
                    p {
                      color:#000;
                      text-transform: ${textTransform ? textTransform : "unset"};
                    }
                    &::after{
                        content: "";
                        position: absolute;
                        background-image: url(${icon});
                        background-position: center;
                        background-size: 100% 100%;
                        background-repeat: no-repeat;
                        transition: 0.3s;
                        width: ${widthIcon};
                        height: ${widthIcon};
                        border-radius:100%;
                        top: 50%;
                        left: 50%;
                        transform: translate(-50%,-50%);
                        z-index: 2;
                    }
                    .btnEffect{
                        position: absolute;
                        border-radius:100%;
                        top: 50%;
                        left: 50%;
                        height:45px;
                        width: 45px;
                        top: 50%;
                        left: 50%;
                        transform: translate(-50%,-50%);
                        background-color:${background};
                        display: block;
                        /* display: none; */
                    }
                    &::before{
                        content: "";
                        position: absolute;
                        height:45px;
                        width: 45px;
                        border-radius:100%;
                        top: 3px;
                        left: 2.2px;
                        background-color: ${backgroundRound};
                        border: 1px solid ${borderColorRound};
                        transition: 0.3s;
                    }
                }
                .btnRound:hover{
                    position: relative;
                    transition: 0.3s;
                    background-color:${backgroundHover};
                }
                .btnRound:hover >.btnEffect{
                    
                }
                .btnRound:hover ::after{
                    transform: translate(-50%,-50%) scale(1.15);
                }
            `}</style>
            </span>
    }


}