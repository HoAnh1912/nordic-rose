import asset from "plugins/assets/asset";
import { COLOR, LOGO, FONTS } from "components/website/constants/Constants";

export default function ButtonNoneBorder({
    text = "Name button",
    handleOutSite,
    fontSize = "14px",
    textColor,
    icon = asset("/images/icon-arrow-black.png"),
    width,
    fonFamily = FONTS.AvenirNextLTProDemi,
}) {

    const color = COLOR;

    const handleClick = () => {
        if (handleOutSite) {
            handleOutSite();
        }
    }

    return <span className="btnNoneBorder" onClick={handleClick}>
       { text }
        <i className="btnEffect"></i>
        <style jsx>{`
            .btnNoneBorder{
                position: relative;
            }
            .btnNoneBorder{
                position: relative;
                color: ${textColor ? textColor : color.BLACK};
                padding: 13px 0px;
                cursor: pointer;
                transition: 0.3s;
                display: inline-block;
                font-size: ${fontSize};
                width: ${width ?  width : "auto"};
                font-family: ${fonFamily};
                z-index: 2;
                font-weight: 500;
            }
            .btnEffect{
                    display: block;
                    position: absolute;
                    background-image: url(${icon});
                    background-position: center;
                    background-size: 100%;
                    background-repeat: no-repeat;
                    transition: 0.3s;
                    width: 15px;
                    height: 12px;
                    left: 106%;
                    top: 50%;
                    transform: translate(0,-50%);
                    z-index: 2;
                }
            .btnNoneBorder:hover{
                padding-left: 10px;
            }
            .btnNoneBorder:hover >.btnEffect{
                transform: translate(50%, -50%);
                opacity: 0;
            }
        `}</style>
    </span>
}