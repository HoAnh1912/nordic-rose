import Slider from "react-slick";
import asset from "plugins/assets/asset";
import { COLOR, FONTS } from "components/website/constants/Constants";
import router, { useRouter } from "next/router";
import { useState, useRef, useEffect } from "react";
import BannerItem from "components/website/banner/BannerItem";

let listImages = [
  {
    subTop: "EXPERTS SINCE 2013",
    titleLeft: "T.O.P Group",
    titleRight: "is a digital agency for sleek technology and kick-ass design.",
    img: asset("/images/demo/banner-01.png"),
    description:
      "Looking for problem solvers? Let us know, we love challenging tasks!",
    video: "https://youtu.be/ea-I4sqgVGY",
    href: "",
  },
  {
    subTop: "",
    hasTag: "Do you know?",
    titleLeft: "T.O.P Group",
    titleRight: "is a digital agency for sleek technology and kick-ass design.",
    img: asset("/images/demo/banner-02.png"),
    description:
      "Looking for problem solvers? Let us know, we love challenging tasks!",
    href: true,
    video:
      "https://www.youtube.com/watch?v=_JSangCM9f4&list=RD_JSangCM9f4&start_radio=1",
  },
];
export default function Banner({
  iconArrow = asset("/images/icon-arrow-black.png"),
  classNames,
  textColorButton = COLOR.TEXT_COLOR,
  iconButton = asset("/images/icon-arrow-black.png"),
  showScroll = false,
}) {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  const onClickVideo = (val) => {};
  return (
    <>
      <Slider {...settings}>
        {listImages ? (
          listImages.map((value, index) => {
            return (
              <div key={index}>
                <BannerItem
                  classNames={classNames}
                  onClickVideo={onClickVideo}
                  data={value}
                  iconButton={iconButton}
                  textColorButton={textColorButton}
                />
              </div>
            );
          })
        ) : (
          <></>
        )}
      </Slider>
      <style jsx>{`
        .scrollTo {
          display: flex;
          justify-content: center;
          align-items: center;
          padding-top: 30px;
          &:hover {
            opacity: 0.8;
          }
          span {
            cursor: pointer;
            padding-right: 5px;

            transition: 0.3s;
          }
          img {
            cursor: pointer;
            width: 10px;
            height: 12px;
            margin-bottom: 2px;
          }
        }
        .contentVideo {
          height: 100vh;
          position: relative;
        }
        .contentSlider {
          padding: 80px 0;
          padding-bottom: 60px;
        }
        .Banner.dark {
          .indexSlide {
            color: ${COLOR.WHITE};
          }
        }
        .indexSlide {
          text-align: center;
          position: relative;
          transition: 0.3s;
          display: flex;
          justify-content: center;
          p {
            width: auto;
            margin-right: 11%;
          }
          span {
            font-family: ${FONTS.ComfortaaLight};
            font-size: 12px;
          }
          .indexActive {
            font-size: 16px;
          }
        }
        @media only screen and (max-width: 768px) {
          .contentSlider {
            padding: 20px 0;
            padding-top: 0;
          }
          .indexSlide {
            display: none;
          }
        }
      `}</style>
    </>
  );
}
