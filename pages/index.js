// import CONFIG from "web.config";
//import MasterPageExample from "components/website/master/MasterPageExample";
// import BasicLayout from "components/diginext/layout/BasicLayout";
// import { useRouter } from "next/router";
// import Header from "components/website/elements/Header";
//import DashkitButton from "components/dashkit/Buttons";
//import { BS } from "components/diginext/elements/Splitters";
import Header from "components/website/header/Header";
import Hero from "components/website/section/hero/Hero";
import Articles from "components/website/section/articles/Articles";
import Footer from "components/website/elements/Footer";

export default function Home(props) {
  return (
    <>
      <Header></Header>
      <Hero></Hero>
      <Articles></Articles>
      <Footer></Footer>
    </>
  );
}
