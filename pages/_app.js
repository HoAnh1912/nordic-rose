import "antd/dist/antd.min.css";
import "styles/global.scss";
import "slick-carousel/slick/slick.css";
import "quill/dist/quill.snow.css";
import { ConfigLive } from "plugins/utils/ConfigLive";
import { useEffect } from "react";

import "styles/header.scss";
import "styles/hero.scss";
import "styles/articles.scss";
import "styles/footer.scss";
import "styles/hero-text.scss";
import "styles/article-detail.scss";

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    ConfigLive.consoleHandle();
    return () => {};
  }, []);

  return <Component {...pageProps} />;
}

export default MyApp;
