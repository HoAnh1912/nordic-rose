import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import AppEvent from '../AppEvent';
import ObjectManager from '../ObjectManager';
import ArrayExtra from 'plugins/utils/ArrayExtra';
import App3D from 'plugins/three/App3D'
import ButtonDebug from './ButtonDebug';

/**
 * 
 * @param {Object} param0 
 * @param {THREE.Scene} param0.scene
 * @returns 
 */
const StatDebug = ({ renderer, scene, ...props }) => {

    const [fps, setfps] = useState(0);
    const [fpsArr, setfpsArr] = useState([]);
    const [info, setinfo] = useState({ memory: {}, render: {} })

    if (props.initListener)
        props.initListener.useSubscription((e) => {
            console.log("StatDebug", e);
            init();

            ButtonDebug.addAction({
                title: "show update",
                type: "show update",
                fn: function () {
                    console.log('scene._listeners.BEFORE_RENDER :>> ', scene._listeners.BEFORE_RENDER);
                    console.log('scene._listeners.AFTER_RENDER :>> ', scene._listeners.AFTER_RENDER);
                    console.log('renderer.info :>> ', renderer.info);
                }
            })
        });


    useEffect(() => {
        // effect
        // init();
        return () => {
            console.log('cleaning...');
            App3D.removeEvent(AppEvent.BEFORE_RENDER, beforeRenderUpdate)
            App3D.removeEvent(AppEvent.AFTER_RENDER, afterRenderUpdate)
    }
    }, [])

    const init = () => {

        renderer = ObjectManager.get("renderer");
        scene = ObjectManager.get("scene");

        App3D.addEvent(AppEvent.BEFORE_RENDER, beforeRenderUpdate);
        App3D.addEvent(AppEvent.AFTER_RENDER, afterRenderUpdate);


    }


    const beforeRenderUpdate = (e) => {


    }


    const afterRenderUpdate = (e) => {
        const list = fpsArr;
        list.push(e.fps);
        if (list.length > 20) list.shift();
        setfpsArr(list);
        const _fps = ArrayExtra.average(list).toFixed(0);
        setfps(_fps);
        setinfo({
            ...renderer.info,
            BEFORE_RENDER: scene._listeners.BEFORE_RENDER ? scene._listeners.BEFORE_RENDER.length : 0,
            AFTER_RENDER: scene._listeners.AFTER_RENDER ? scene._listeners.AFTER_RENDER.length : 0,
        });

    }


    return (
        <>
            <style jsx>{`
                .holderStats{
                    bottom: 0px;
                    color:white;
                    padding: 15px;
                    user-select: none;
                    pointer-events: none;
                    flex: 1;
                    display: inline-table;
                    background: #000000aa;

                    p{
                        color:white;
                        white-space:nowrap;
                    }
                }
            `}</style>

            <div className='holderStats'>
                <p>{`fps: ${fps}`}</p>
                <p>{`drawCalls: ${info.render.calls}`}</p>
                <p>{`triangles: ${info.render.triangles}`}</p>
                <p>{`geometries: ${info.memory.geometries}`}</p>
                <p>{`textures: ${info.memory.textures}`}</p>
                <p>{`BEFORE_RENDER: ${info.BEFORE_RENDER}`}</p>
                <p>{`AFTER_RENDER: ${info.AFTER_RENDER}`}</p>
            </div>
        </>
    )
}

StatDebug.propTypes = {

}

export default StatDebug
